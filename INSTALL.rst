***********************************
Practical Data Analysis with Python
***********************************

:Author:  Dr. Andreas Hilboll
:E-mail:  hilboll@uni-bremen.de
:Date:    Winter term 2019


Setting up a Python environment on your computer
================================================

There are several different Python *distributions*. I recommend to use
the Python 3 version of the `Anaconda Python Distribution
<https://www.anaconda.com/download/>`__. The installation is very well
`documented
<https://docs.anaconda.com/anaconda/install>`__. Additional help can
be found `here
<http://swcarpentry.github.io/python-novice-gapminder/setup/>`__.

Also, I recommend to use `conda environments
<https://docs.conda.io/projects/conda/en/latest/user-guide/getting-started.html#managing-environments>`__
within Anaconda.

Usually, the installation of Anaconda Python is pretty
straightforward, but can sometimes be tricky depending on your
computer environment.

   If you run into problems with the installation, feel free to
   contact me and I'll try to help out!


Using the command line
----------------------

You can create a new environment from the *Anaconda Navigator*, or on
the command line with the command

.. code:: shell

   conda create --name pdap2019 python=3 anaconda

This will create a new environment called *pdap2019*, with the most
recent *Python3* release.  Also, a number of important packages for
scientific computing will be installed into that environment.

Finally, you can now start the Jupyter interface with the command

.. code:: shell

   jupyter-lab


Using the Anaconda Navigator
----------------------------

The `Anaconda Navigator
<https://docs.anaconda.com/anaconda/navigator/>`__ is a desktop
graphical user interface to your Anaconda installation.

After installation of the Anaconda distrbibution, there should be an
entry *Anaconda Navigator* in your computer.  Click it to launch the
application.  You can follow the documentation at the Anaconda
Navigator website to perform the following tasks:

1. Create a new environment ``pdap2019`` based on *Python3*.
2. Launch the ``JupyterLab`` app in that environment.


Finish
------

No matter if you used the command line or the Anaconda Navigator: when launching JupyterLab, your web browser should open to the Jupyter interface.
