****************
Reading material
****************

Python
======

The Jupyter interface
---------------------

- There is a nice introduction to the JupyterLab interface including short videos at https://jupyterlab.readthedocs.io/en/stable/user/interface.html
- Another introduction including exercises is available at https://swcarpentry.github.io/python-novice-gapminder/01-run-quit/


The Python environment
----------------------

- `Managing conda environments
  <https://conda.io/docs/user-guide/tasks/manage-environments.html>`__
- When disk space is an issue, you can use the `conda clean
  <https://docs.conda.io/projects/conda/en/latest/commands/clean.html>`__
  command to free up used space.  ``conda clean -t -p`` should remove
  all downloaded intermediate files from your conda installation
  without actually removing any installed packages (it will only
  remove temporary installation files).


The Python language in general
------------------------------

- `The Hitchhiker’s Guide to Python <http://docs.python-guide.org/en/latest/>`__ is a great, free, general book about using Python.
- `A Whirlwind Tour of Python <https://nbviewer.jupyter.org/github/jakevdp/WhirlwindTourOfPython/blob/master/Index.ipynb>`__ is a notebook-based introduction into Python basics, highly recommended.


Python in science
-----------------

Books
~~~~~

- A great book about Python in data analysis is the `Python Data
  Science Handbook
  <https://jakevdp.github.io/PythonDataScienceHandbook/>`__ by Jake
  van der Plas.  It is also available in printed form.

- Another great book, mostly about the pandas_ library, is *Python for
  data analysis* by Wes McKinney.  As a university student, you have
  free online access after a one-time registration.  Go to the
  university library's `page about this book
  <https://suche.suub.uni-bremen.de/peid=B101493646&LAN=DE&CID=6578963&index=L&Hitnr=6&dtyp=O&rtyp=a>`__,
  click the *VOLLTEXT* link, and register.  After that, you have full
  access to the book.

.. _pandas: https://pandas.pydata.org/


Lectures
~~~~~~~~

Two good general resources about Python in science are:

- `Python Scientific Lecture Notes <https://scipy-lectures.github.io/index.html>`__
- `J. R. Johansson's Lectures on Scientific Computing with Python <http://nbviewer.ipython.org/github/jrjohansson/scientific-python-lectures/tree/master>`__


Cookbooks / Example galleries
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- One great resource is https://ipython-books.github.io/


Pandas
~~~~~~

- The `Pandas cookbook <https://github.com/jvns/pandas-cookbook>`__ by `Julia Evans <http://jvns.ca/>`__ is a nice overview about the capabilities of Pandas.


Plotting
~~~~~~~~

- The `matplotlib documentation <https://matplotlib.org/>`__ is very extensive.
- Especially take a look at the `matplotlib example gallery <https://matplotlib.org/gallery/>`__.  If you know what you want to achieve, you can check the gallery for a plot that does the same / a similar thing, and look at the code to achieve that.
- The matplotlib lecture was based on `chapter 4 of the Python Data Science Handbook <https://jakevdp.github.io/PythonDataScienceHandbook/04.00-introduction-to-matplotlib.html>`__.
- Also helpful can be the `matplotlib lecture from SciPy lecture notes <http://scipy-lectures.org/intro/matplotlib/>`__.
- A list of colormaps in matplotlib can be found in the `colormap reference <https://matplotlib.org/gallery/color/colormap_reference.html>`__ (but also check out the `corresponding section from the Handbook <https://jakevdp.github.io/PythonDataScienceHandbook/04.07-customizing-colorbars.html>`__.
- The full list of customizable matplotlib settings can be found in `Customizing matplotlib <https://matplotlib.org/users/customizing.html>`__.
- For a high-level interactive plotting experience in the Notebook, check out hvPlot_.

.. _hvPlot: https://hvplot.pyviz.org/
  
  
Comparison to other languages
-----------------------------

-  If you know Matlab, then `Numpy for Matlab
   users <https://docs.scipy.org/doc/numpy/user/numpy-for-matlab-users.html>`__
   might help.
-  The `Thesaurus of Mathematical
   Languages <http://mathesaurus.sourceforge.net/>`__ gives
   "translations" between Python and Matlab, R, Numeric, and IDL.
   **CAUTION:** This document is rather old, some details might not out
   of date.


Free Jupyter Notebook hosting
=============================

In cases where you don't have access to your own Jupyter account, but you still want to work in the Jupyter Notebook, you can use of the many free services:

- https://colab.research.google.com/notebooks/welcome.ipynb
- https://notebooks.azure.com/  (didn't work for me)
- https://www.ibm.com/cloud/watson-studio
- https://cocalc.com/
- https://codeocean.com/

All these links come from `https://blog.ouseful.info/2019/01/04/more-than-ten-free-hosted-jupyter-notebook-environments-you-can-try-right-now/ <this blog article>`__.


Computer basics
===============

- If you need assistance in understanding what files / directories are, and how to navigate them, `this Software Carpentry lecture <https://swcarpentry.github.io/shell-novice/02-filedir/index.html>`__ might be useful.
- The JupyterLab documentation has a `section about file handling <https://jupyterlab.readthedocs.io/en/stable/user/files.html>`__.


Git
===============

There are a number of free Git resources available on the internet, most
notably

- `Pro Git <https://git-scm.com/book/en/v2>`__, a good reference book,
   also available in other languages (`Español
   <https://git-scm.com/book/es>`__, `Français
   <https://git-scm.com/book/fr/v2>`__, `日本語
   <https://git-scm.com/book/ja>`__, `한국어
   <https://git-scm.com/book/ko>`__ `Nederlands
   <https://git-scm.com/book/nl>`__, `Русский
   <https://git-scm.com/book/ru>`__, `Українська
   <https://git-scm.com/book/uk>`__, `简体中文
   <https://git-scm.com/book/zh>`__). Partial translations exist for
   other languages as well; check the sidebar of the English version.
-  `Das Git Buch <http://gitbu.ch/>`__ is another good reference book,
   written in German.
-  The best resource for finding answers to specific questions is
   undoubtedly `stackoverflow.com <https://stackoverflow.com/>`__.

